package hael.goodexchange.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String userId;

    @Column(nullable = false)
    private String currencyId;

    private BigDecimal amount;

    public long getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
