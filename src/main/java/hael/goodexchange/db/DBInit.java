package hael.goodexchange.db;

import org.springframework.boot.CommandLineRunner;
import hael.goodexchange.model.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class DBInit implements CommandLineRunner {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public DBInit(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        this.userRepository.deleteAll();

        User john = new User("John", passwordEncoder.encode("john123"), "USER");
        User susanna = new User("Susanna", passwordEncoder.encode("sus1"), "USER");
        User sydney = new User("Sydney", passwordEncoder.encode("sys2"), "USER");
        List<User> users = Arrays.asList(john, susanna, sydney);

        this.userRepository.saveAll(users);
    }
}